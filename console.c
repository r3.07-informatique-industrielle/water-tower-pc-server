#include <stdio.h>
#include <windows.h>
#include <conio.h>
#include "console.h"

HANDLE hConsole_c;

void ConsoleInit()
{
  hConsole_c = CreateConsoleScreenBuffer(GENERIC_WRITE, 0, NULL, CONSOLE_TEXTMODE_BUFFER, NULL);
  SetConsoleActiveScreenBuffer(hConsole_c);
  SetConsoleOutputCP(437);
  CONSOLE_CURSOR_INFO cci;
  cci.dwSize = 1;
  cci.bVisible = FALSE;
  SetConsoleCursorInfo(hConsole_c, &cci);
}

void print_at(COORD pos, const char *str)
{
  DWORD dwBytesWritten = 0;
  WriteConsoleOutputCharacter(hConsole_c, str, strlen(str), pos, &dwBytesWritten);
}

void bargraph_at(COORD pos, double value)
{
  char bar[51];
  int entier = (int)(value);

  for (int i = 0; i < entier; i++)
  {
    bar[i] = (char)178;
  }

  int frac = (int)((value - entier) * 4);
  if (frac > 2)
  {
    bar[entier] = (char)177;
    entier += 1;
  }
  else if (frac > 1)
  {
    bar[entier] = (char)176;
    entier += 1;
  }

  for (int i = entier; i < 50; i++)
  {
    bar[i] = ' ';
  }

  bar[50] = 0;

  print_at(pos, bar);
}

void ConsoleClose()
{
  CloseHandle(hConsole_c);
}
