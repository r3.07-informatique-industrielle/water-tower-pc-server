#include <windows.h>
#include <time.h>
#include "arduino.h"
#include <stdio.h>

PinIO _digital[256];

void pinMode(byte p, byte mode)
{
  _digital[p].mode = 0x01 | mode;

  _digital[p].ivalue = 0;
  _digital[p].dvalue = 0.0;

  _digital[p].nb = 0;
  _digital[p].time = 0.0;
  _digital[p].duration = 0.0;
  _digital[p].start = 0;
  _digital[p].raising = 0;
  _digital[p].memory = 0;
}

/* WRITE */

void digitalWrite(unsigned int p, int value)
{
  if (p < 0 || p > 255)
  {
    printf("Pin %d is out of Range.", p);
    return;
  }

  if (!(_digital[p].mode & 0x01))
  {
    // printf("Pin %d is not active.", p);
    return;
  }

  if (!(_digital[p].mode & IO_OUTPUT && _digital[p].mode & DIGITAL))
  {
    printf("Pin %d is not a digital input.", p);
    return;
  }

  unsigned long m = millis();

  if (value != _digital[p].ivalue)
  {
    _digital[p].time = _digital[p].time > 5000 ? 0 : 5000 - _digital[p].time;
  }
  else
  {
    _digital[p].time += m - _digital[p].start;
    _digital[p].start = m;
  }

  if (value && !_digital[p].ivalue)
  {
    _digital[p].start = millis();
    _digital[p].nb += 1;
  }
  else if (value)
  {
    _digital[p].duration += m - _digital[p].start;
  }

  _digital[p].ivalue = value;
}

void analogWrite(unsigned int p, double value)
{
  if (p < 0 || p > 31)
  {
    printf("Pin %d is out of Range.", p);
    return;
  }

  if (!(_digital[p].mode & 0x01))
  {
    // printf("Pin %d is not active.", p);
    return;
  }

  if (!(_digital[p].mode & IO_OUTPUT) || !(_digital[p].mode & ANALOG))
  {
    printf("Pin %d is not a analog input.", p);
    return;
  }

  _digital[p].dvalue = value;
}

/* READ */

int digitalRead(int p)
{
  return ((_digital[p].mode & IO_INPUT) && (_digital[p].mode & DIGITAL) && (_digital[p].mode & 0x01)) ? _digital[p].ivalue : 0;
}

double analogRead(int p)
{
  return ((_digital[p].mode & IO_INPUT) && (_digital[p].mode & ANALOG) && (_digital[p].mode & 0x01)) ? _digital[p].dvalue : 0.0;
}

/* Time */

unsigned long millis()
{
  struct timespec now;
  clock_gettime(CLOCK_REALTIME, &now);
  return ((unsigned long)now.tv_sec) * 1000 + ((unsigned long)now.tv_nsec) / 1000000;
}
