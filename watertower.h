// KEYBOARD
typedef struct
{
  char input;
  int vKey;
} KeyboardIO;

#define NB_KEYBOARD 10
extern KeyboardIO _keyboard[NB_KEYBOARD];

void ChateauEauInitialValues(unsigned short mode);
void ChateauEauProcess(unsigned short mode);
void ChateauEauInit(unsigned short mode);
