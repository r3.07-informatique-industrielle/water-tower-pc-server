#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include "watertower.h"
#include "pin.h"
#include "arduino.h"
#include "console.h"

unsigned long t_start, t_backup;

KeyboardIO _keyboard[NB_KEYBOARD];

float TankInitalValue[4] = {7, 1.5, 9, 7};

void ChateauEauInitialValues(unsigned short mode)
{
  t_start = t_backup = millis();

  _digital[IN_TANK_LEVEL].dvalue = _digital[IN_TANK_MAX].dvalue = _digital[IN_TANK_MIN].dvalue = TankInitalValue[mode];

  _digital[OUT_FLOW_PER_PUMP].dvalue = 65.0;
  _digital[OUT_FLOW_OUT_MAX].dvalue = 100.0;

  _digital[OUT_LEVEL_MIN].dvalue = 2;
  _digital[OUT_LEVEL_LOW].dvalue = 6;
  _digital[OUT_LEVEL_HIGH].dvalue = 8;
  _digital[OUT_LEVEL_MAX].dvalue = 9.5;
}

void ChateauEauTemplate()
{
  print_at((COORD){0, 0}, "\xC9\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xBB");
  print_at((COORD){0, 1}, "\272Ch\203teau d'eau (09/2023)                                   \xBA");
  print_at((COORD){0, 2}, "\xC7\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC2\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xB6");
  print_at((COORD){0, 3}, "\xBA BP 1               \xB3 Pompe 1                             \xBA");
  print_at((COORD){0, 4}, "\xBA BP 2               \xB3 Pompe 2                             \xBA");
  print_at((COORD){0, 5}, "\xBA BP 3               \xB3 Pompe 3                             \xBA");
  print_at((COORD){0, 6}, "\xBA BP 4               \xB3 Pompe 4                             \xBA");
  print_at((COORD){0, 7}, "\xC7\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC1\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xB6");
  print_at((COORD){0, 8}, "\272D\202bit en sortie                                           \xBA");
  print_at((COORD){0, 9}, "\272D\202bit en entree                                           \xBA");
  print_at((COORD){0, 10}, "\xBAVolume dans le r\202servoir                                  \xBA");
  print_at((COORD){0, 11}, "\xC7\xC4\xC4\xC4\xC4\xC2\xC4\xC4\xC4\xC4\xC2\xC4\xC4\xC4\xC4\xC2\xC4\xC4\xC4\xC4\xC2\xC4\xC4\xC4\xC4\xC2\xC4\xC4\xC4\xC4\xC2\xC4\xC4\xC4\xC4\xC2\xC4\xC4\xC4\xC4\xC2\xC4\xC4\xC4\xC4\xC2\xC4\xC4\xC4\xC4\xC4\xC2\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xB6");
  print_at((COORD){0, 12}, "\xBA                                                  \xB3       \xBA");
  print_at((COORD){0, 13}, "\xC7\xC4\xC4\xC4\xC4\xC1\xC4\xC4\xC4\xC4\xC1\xC4\xC4\xC4\xC4\xC1\xC4\xC4\xC4\xC4\xC1\xC4\xC4\xC4\xC4\xC1\xC4\xC4\xC4\xC4\xC1\xC4\xC4\xC4\xC4\xC1\xC4\xC4\xC4\xC4\xC1\xC4\xC4\xC4\xC4\xC1\xC4\xC4\xC4\xC4\xC4\xC1\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xB6");
  print_at((COORD){0, 14}, "\xBA min                                                      \xBA");
  print_at((COORD){0, 15}, "\xC7\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC2\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC2\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xB6");
  print_at((COORD){0, 16}, "\xBA Mode             \xB3 Grafcet    \xB3 min          max         \xBA");
  print_at((COORD){0, 17}, "\xC8\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\317\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\317\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xBC");
}

void ChateauEauKeyboard()
{
  _keyboard[0].vKey = '1';
  _keyboard[0].input = IN_KEYBOARD_1;

  _keyboard[1].vKey = '2';
  _keyboard[1].input = IN_KEYBOARD_2;

  _keyboard[2].vKey = '3';
  _keyboard[2].input = IN_KEYBOARD_3;

  _keyboard[3].vKey = '4';
  _keyboard[3].input = IN_KEYBOARD_4;

  _keyboard[4].vKey = 'A';
  _keyboard[4].input = IN_KEYBOARD_A;

  _keyboard[5].vKey = 'X';
  _keyboard[5].input = IN_KEYBOARD_X;

  _keyboard[6].vKey = '7';
  _keyboard[6].input = IN_KEYBOARD_7;

  _keyboard[7].vKey = '8';
  _keyboard[7].input = IN_KEYBOARD_8;

  _keyboard[8].vKey = '9';
  _keyboard[8].input = IN_KEYBOARD_9;

  _keyboard[9].vKey = '0';
  _keyboard[9].input = IN_KEYBOARD_0;

  for (int i = 0; i < NB_KEYBOARD; i++)
  {
      _digital[_keyboard[i].input].mode = OP_DIGITAL_READ;
  }
}

void ChateauEauDisplay(unsigned long t)
{
  char str[200];

  sprintf(str, "%5.1f s", (t - t_start) / 1000.0);
  print_at((COORD){50, 1}, str);

  for (int i = OUT_PUMP_1; i <= OUT_PUMP_4; i++)
  {
      sprintf(str, " %c    %5.1f s   %dx", _digital[i].mode & 0x01 ? _digital[i].ivalue ? 'M' : '.' : 'X', _digital[i].duration / 1000.0, _digital[i].nb);
      print_at((COORD){30, (short)(i - OUT_PUMP_1 + 3)}, str);
  }

  for (int i = IN_KEYBOARD_1; i <= IN_KEYBOARD_4; i++)
  {
    print_at((COORD){10, (short)(i + 3)}, _digital[_keyboard[i].input].ivalue ? "1" : "0");
  }

  sprintf(str, "%3d    l/s", (int)_digital[IN_FLOW_OUT].dvalue);
  print_at((COORD){28, 8}, str);

  sprintf(str, "%3d    l/s", (int)_digital[IN_FLOW_IN].dvalue);
  print_at((COORD){28, 9}, str);

  sprintf(str, "%5.2lf m3    (%+d l/s)", _digital[IN_TANK_LEVEL].dvalue, (int)_digital[IN_FLOW_DIF].dvalue);
  print_at((COORD){29, 10}, str);

  bargraph_at((COORD){1, 12}, _digital[IN_TANK_LEVEL].dvalue * 5);

  if (_digital[IN_SENSOR_LOW].mode & 0x01)
  {
    sprintf(str, "          min %d %dx       low %d %dx      %d %dx    %d %dx", _digital[IN_SENSOR_MIN].ivalue, _digital[IN_SENSOR_MIN].nb, _digital[IN_SENSOR_LOW].ivalue, _digital[IN_SENSOR_LOW].nb, _digital[IN_SENSOR_HIGH].ivalue, _digital[IN_SENSOR_HIGH].nb, _digital[IN_SENSOR_MAX].ivalue, _digital[IN_SENSOR_MAX].nb);
  }
  else
  {
    sprintf(str, "          min %d %dx       low X         %d %dx    %d %dx", _digital[IN_SENSOR_MIN].ivalue, _digital[IN_SENSOR_MIN].nb, _digital[IN_SENSOR_HIGH].ivalue, _digital[IN_SENSOR_HIGH].nb, _digital[IN_SENSOR_MAX].ivalue, _digital[IN_SENSOR_MAX].nb);
  }
  print_at((COORD){1, 14}, str);

  print_at((COORD){7, 16}, _digital[OUT_DISPLAY_MODE].ivalue ? "Automatique" : "Manuel     ");

  sprintf(str, "%d", _digital[OUT_DISPLAY_GRAFCET].ivalue);
  print_at((COORD){29, 16}, str);

  sprintf(str, "%4.2lf m3", _digital[IN_TANK_MIN].dvalue);
  print_at((COORD){38, 16}, str);

  sprintf(str, "%4.2lf m3", _digital[IN_TANK_MAX].dvalue);
  print_at((COORD){51, 16}, str);
}

void ChateauEauInit(unsigned short mode)
{
  ConsoleInit();

  ChateauEauKeyboard();

  pinMode(IN_KEYBOARD_1, IO_INPUT | DIGITAL);
  pinMode(IN_KEYBOARD_2, IO_INPUT | DIGITAL);
  pinMode(IN_KEYBOARD_3, IO_INPUT | DIGITAL);
  pinMode(IN_KEYBOARD_4, IO_INPUT | DIGITAL);
  pinMode(IN_KEYBOARD_A, IO_INPUT | DIGITAL);

  pinMode(IN_KEYBOARD_7, IO_INPUT | DIGITAL);
  pinMode(IN_KEYBOARD_8, IO_INPUT | DIGITAL);
  pinMode(IN_KEYBOARD_9, IO_INPUT | DIGITAL);
  pinMode(IN_KEYBOARD_0, IO_INPUT | DIGITAL);
  pinMode(IN_KEYBOARD_X, IO_INPUT | DIGITAL);

  pinMode(IN_SENSOR_MIN, IO_INPUT | DIGITAL);
  pinMode(IN_SENSOR_LOW, IO_INPUT | DIGITAL);
  pinMode(IN_SENSOR_HIGH, IO_INPUT | DIGITAL);
  pinMode(IN_SENSOR_MAX, IO_INPUT | DIGITAL);

  pinMode(IN_TANK_LEVEL, IO_INPUT | ANALOG);
  pinMode(IN_FLOW_OUT, IO_INPUT | ANALOG);
  pinMode(IN_FLOW_IN, IO_INPUT | ANALOG);
  pinMode(IN_FLOW_DIF, IO_INPUT | ANALOG);
  pinMode(IN_TANK_MIN, IO_INPUT | ANALOG);
  pinMode(IN_TANK_MAX, IO_INPUT | ANALOG);
  pinMode(IN_FLOW_CAP, IO_INPUT | ANALOG);
  pinMode(IN_FLOW_1, IO_INPUT | ANALOG);
  pinMode(IN_FLOW_2, IO_INPUT | ANALOG);
  pinMode(IN_FLOW_3, IO_INPUT | ANALOG);
  pinMode(IN_FLOW_4, IO_INPUT | ANALOG);

  pinMode(OUT_PUMP_1, IO_OUTPUT | DIGITAL);
  pinMode(OUT_PUMP_2, IO_OUTPUT | DIGITAL);
  pinMode(OUT_PUMP_3, IO_OUTPUT | DIGITAL);
  pinMode(OUT_PUMP_4, IO_OUTPUT | DIGITAL);

  pinMode(OUT_DISPLAY_MODE, IO_OUTPUT | DIGITAL);
  pinMode(OUT_DISPLAY_GRAFCET, IO_OUTPUT | DIGITAL);

  pinMode(OUT_LEVEL_MIN, IO_OUTPUT | ANALOG);
  pinMode(OUT_LEVEL_LOW, IO_OUTPUT | ANALOG);
  pinMode(OUT_LEVEL_HIGH, IO_OUTPUT | ANALOG);
  pinMode(OUT_LEVEL_MAX, IO_OUTPUT | ANALOG);
  pinMode(OUT_FLOW_PER_PUMP, IO_OUTPUT | ANALOG);
  pinMode(OUT_FLOW_OUT_MAX, IO_OUTPUT | ANALOG);

  pinMode(OUT_BEEP, IO_OUTPUT | DIGITAL);

  _digital[OUT_PUMP_1].error = 30;
  _digital[OUT_PUMP_1].efficacite = 1.0;
  _digital[OUT_PUMP_1].time = UINT_MAX;

  _digital[OUT_PUMP_2].error = 30;
  _digital[OUT_PUMP_2].efficacite = 0.72;
  _digital[OUT_PUMP_2].time = UINT_MAX;

  _digital[OUT_PUMP_3].error = 10;
  _digital[OUT_PUMP_3].efficacite = 1.0;
  _digital[OUT_PUMP_3].time = UINT_MAX;

  _digital[OUT_PUMP_4].error = 30;
  _digital[OUT_PUMP_4].efficacite = 1.0;
  _digital[OUT_PUMP_4].time = UINT_MAX;

  ChateauEauInitialValues(mode);

  ChateauEauTemplate();
}

double Moteur(int i) {

  double vitesse = 1.0;
  double t = _digital[i].time / 5000.0;

  if (_digital[i].ivalue)
  {
    if (_digital[i].time < 2500)
    {
      vitesse = 4 * pow(t, 3.0);
    }
    else if (_digital[i].time < 5000)
    {
      vitesse = 1.0 - pow(2 - 2 * t, 3) / 2.0;
    }
    else
    {
      vitesse = 1.0 + 1.0 / (_digital[i].error * 2.0) - rand() / (double)RAND_MAX / _digital[i].error;
    }
  }
  else
  {
    if (_digital[i].time < 2500)
    {
      vitesse = 1 - 4 * pow(t, 3.0);
    }
    else if (_digital[i].time < 5000)
    {
      vitesse = pow(2 - 2 * t, 3) / 2.0;
      //  vitesse = 1 - pow(t, 4.0);
    }
    else
    {
      vitesse = 0.0;
    }
  }

  return _digital[OUT_FLOW_PER_PUMP].dvalue * _digital[i].efficacite * vitesse;
}


void ChateauEauProcess(unsigned short mode)
{
  // *****
  unsigned long t = millis();
  unsigned long delta_t = t - t_backup;

  // ***** FLOW OUT
  if (_digital[IN_TANK_LEVEL].dvalue > 1.0)
  {
    double alea = ((int)(t / 100.0) % 600) * 3 / 1800.0 * PI;
    _digital[IN_FLOW_OUT].dvalue = 100 + cos(alea) * cos(alea) * _digital[OUT_FLOW_OUT_MAX].dvalue;
  }
  else
  {
    if (_digital[IN_FLOW_CAP].dvalue == 0.0)
      _digital[IN_FLOW_CAP].dvalue = _digital[IN_FLOW_OUT].dvalue;
    _digital[IN_FLOW_OUT].dvalue = _digital[IN_FLOW_CAP].dvalue * _digital[IN_TANK_LEVEL].dvalue;
  }

  // ***** FLOW IN
  _digital[IN_FLOW_IN].dvalue = 0;
  for (int i = OUT_PUMP_1; i <= OUT_PUMP_4; i++)
  {
    _digital[i - 4].dvalue = Moteur(i);
    _digital[IN_FLOW_IN].dvalue += _digital[i - 4].dvalue;
  }

  _digital[IN_FLOW_DIF].dvalue = _digital[IN_FLOW_IN].dvalue - _digital[IN_FLOW_OUT].dvalue;

  // ***** TANK LEVEL
  _digital[IN_TANK_LEVEL].dvalue += (_digital[IN_FLOW_IN].dvalue - _digital[IN_FLOW_OUT].dvalue) / 1000.0 * delta_t / 1000.0;
  if (_digital[IN_TANK_LEVEL].dvalue > 10.0)
    _digital[IN_TANK_LEVEL].dvalue = 10.0;

  if (_digital[IN_TANK_LEVEL].dvalue > _digital[IN_TANK_MAX].dvalue)
    _digital[IN_TANK_MAX].dvalue = _digital[IN_TANK_LEVEL].dvalue;

  if (_digital[IN_TANK_LEVEL].dvalue < _digital[IN_TANK_MIN].dvalue)
    _digital[IN_TANK_MIN].dvalue = _digital[IN_TANK_LEVEL].dvalue;

  // **** KEYBOARD
  if (_digital[IN_KEYBOARD_X].raising)
  {
    _digital[IN_SENSOR_LOW].mode = _digital[IN_SENSOR_LOW].mode ^ 0x01;
  }

  for (int i = IN_KEYBOARD_7; i <= IN_KEYBOARD_0; i++)
  {
    if (_digital[i].raising)
    {
      unsigned char p = i + (OUT_PUMP_1 - IN_KEYBOARD_7);
      _digital[p].mode ^= 0x01;
      if (!(_digital[p].mode & 0x01)) {
        _digital[p].ivalue = 0;
        _digital[p].dvalue = 0.0;
      }
    }
  }

  // **** SENSOR
  int test;

  test = (_digital[IN_TANK_LEVEL].dvalue > _digital[OUT_LEVEL_MIN].dvalue);
  if (_digital[IN_SENSOR_MIN].ivalue != test)
  {
    if (test == 0)
    {
      _digital[IN_SENSOR_MIN].nb += 1;
    }
    _digital[IN_SENSOR_MIN].ivalue = test;
  }

  test = _digital[IN_TANK_LEVEL].dvalue > _digital[OUT_LEVEL_LOW].dvalue && _digital[IN_SENSOR_LOW].mode & 0x01;
  if (_digital[IN_SENSOR_LOW].ivalue != test)
  {
    if (test == 0)
    {
      _digital[IN_SENSOR_LOW].nb += 1;
    }
    _digital[IN_SENSOR_LOW].ivalue = test;
  }

  test = _digital[IN_TANK_LEVEL].dvalue > _digital[OUT_LEVEL_MAX].dvalue;
  if (_digital[IN_SENSOR_MAX].ivalue != test)
  {
    if (test == 1)
    {
      _digital[IN_SENSOR_MAX].nb += 1;
    }
    _digital[IN_SENSOR_MAX].ivalue = test;
  }

  test = _digital[IN_TANK_LEVEL].dvalue > _digital[OUT_LEVEL_HIGH].dvalue;
  if (_digital[IN_SENSOR_HIGH].ivalue != test)
  {
    if (test == 1)
    {
      _digital[IN_SENSOR_HIGH].nb += 1;
    }
    _digital[IN_SENSOR_HIGH].ivalue = test;
  }

  ChateauEauDisplay(t);

  t_backup = t;
}
