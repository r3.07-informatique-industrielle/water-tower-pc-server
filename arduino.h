/* From Arduino.h */

#define HIGH 0x1
#define LOW 0x00

#define IO_INPUT 0x02
#define IO_OUTPUT 0x04

#define DIGITAL 0x08
#define ANALOG 0x10

#define PI 3.1415926535897932384626433832795
#define HALF_PI 1.5707963267948966192313216916398

#define OP_DIGITAL_READ 0
#define OP_DIGITAL_WRITE 1
#define OP_ANALOG_READ 2
#define OP_ANALOG_WRITE 3
#define OP_PIN_MODE 4

typedef struct PinIO
{
  unsigned char mode;
  int ivalue;
  double dvalue;
  int error; // % = 1 /error ex 1 / 20 = 5 %
  double efficacite; // 0 - 100%
  unsigned long start;
  unsigned long time;
  unsigned long duration;
  unsigned int nb; // COUNT OF ACTIVATION
  int memory;
  unsigned char raising;
  unsigned char falling;
} PinIO;

extern PinIO _digital[256];

void pinMode(unsigned char, unsigned char);

void digitalWrite(unsigned int pin, int value);
void analogWrite(unsigned int pin, double value);
int digitalRead(int pin);
double analogRead(int pin);

unsigned long millis();
