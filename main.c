#include <windows.h>
#include <stdio.h>
#include <conio.h>

#include "main.h"
#include "arduino.h"
#include "console.h"
#include "watertower.h"

#define BUFSIZE 125

int client_connected = 1;
unsigned short mode = 0;

int main(int argc, char *argv[])
{
  BOOL fConnected = FALSE;
  DWORD dwThreadId = 0;
  HANDLE hPipe = INVALID_HANDLE_VALUE, hThread = NULL;
  LPCTSTR lpszPipename = "\\\\.\\pipe\\geii";

  if (argc > 1)
  {
    mode = argv[1][0] - 48;
  }

  CreateMaquetteThread();

  // The main loop creates an instance of the named pipe and
  // then waits for a client to connect to it. When the client
  // connects, a thread is created to handle communications
  // with that client, and this loop is free to wait for the
  // next client connect request. It is an infinite loop.

  while (1)
  {
    printf("\nServeur Maquette GEII\nEn attente d'un nouvelle connexion sur %s\nAppuyer sur ctrl+c pour quitter totalement le programme\n", lpszPipename);
    hPipe = CreateNamedPipe(
        lpszPipename,             // pipe name
        PIPE_ACCESS_DUPLEX,       // read/write access
        PIPE_TYPE_BYTE |          // message type pipe
        PIPE_READMODE_BYTE |      // message-read mode
        PIPE_WAIT,                // blocking mode
        PIPE_UNLIMITED_INSTANCES, // max. instances
        BUFSIZE,                  // output buffer size
        BUFSIZE,                  // input buffer size
        0,                        // client time-out
        NULL);                    // default security attribute

    if (hPipe == INVALID_HANDLE_VALUE)
    {
      printf("CreateNamedPipe failed, GLE=%lu.\n", GetLastError());
      return -1;
    }

    // Wait for the client to connect; if it succeeds,
    // the function returns a nonzero value. If the function
    // returns zero, GetLastError returns ERROR_PIPE_CONNECTED.

    fConnected = ConnectNamedPipe(hPipe, NULL) ? TRUE : (GetLastError() == ERROR_PIPE_CONNECTED);

    if (fConnected)
    {
      printf("Connexion d'un client en cours ...\n");

      // Create a thread for this client.
      hThread = CreateThread(
          NULL,          // no security attribute
          0,             // default stack size
          PipeThread,    // thread proc
          (LPVOID)hPipe, // thread parameter
          0,             // not suspended
          &dwThreadId);  // returns thread ID

      if (hThread == NULL)
      {
        printf("CreateThread failed, GLE=%lu.\n", GetLastError());
        return -1;
      }
      else
      {
        CloseHandle(hThread);
      }
    }
    else
    {
      // The client could not connect, so close the pipe.
      CloseHandle(hPipe);
    }
  }

  return 0;
}

DWORD WINAPI PipeThread(LPVOID lpvParam)
// This routine is a thread processing function to read from and reply to a client
// via the open pipe connection passed from the main loop. Note this allows
// the main loop to continue executing, potentially creating more threads of
// of this procedure to run concurrently, depending on the number of incoming
// client connections.
{
  DWORD cbBytesRead = 0, cbReplyBytes = 0, cbWritten = 0;
  BOOL fSuccess = FALSE;
  HANDLE hPipe = NULL;

  // Do some extra error checking since the app will keep running even if this
  // thread fails.

  if (lpvParam == NULL)
  {
    printf("\nERROR - Pipe Server Failure:\n");
    printf("   PipeThread got an unexpected NULL value in lpvParam.\n");
    printf("   PipeThread exiting.\n");
    return (DWORD)-1;
  }

  // The thread's parameter is a handle to a pipe object instance.
  hPipe = (HANDLE)lpvParam;

  ChateauEauInitialValues(mode);

  // Loop until done reading
  while (1)
  {
    datastruct data;

    fSuccess = ReadFile(
        hPipe,              // handle to pipe
        &data,              // buffer to receive data
        sizeof(datastruct), // size of buffer
        &cbBytesRead,       // number of bytes read
        NULL);              // not overlapped I/O

    if (!fSuccess || cbBytesRead == 0)
    {
      if (GetLastError() == ERROR_BROKEN_PIPE)
      {
        printf("Client d�connect�.\n");
        client_connected = 0;
      }
      else
      {
        printf("Erreur de lecture, GLE=%lu.\n", GetLastError());
      }
      break;
    }

    if (data.method == OP_DIGITAL_READ)
    {
      data.dvalue = _digital[data.registre].ivalue;

      fSuccess = WriteFile(
          hPipe,              // handle to pipe
          &data,              // buffer to write from
          sizeof(datastruct), // number of bytes to write
          &cbWritten,         // number of bytes written
          NULL);              // not overlapped I/O

      if (!fSuccess || cbWritten != sizeof(datastruct))
      {
        printf("Erreur d'�criture, GLE=%lu.\n", GetLastError());
        break;
      }
    }
    else if (data.method == OP_DIGITAL_WRITE)
    {
      digitalWrite(data.registre, data.dvalue);
    }
    else if (data.method == OP_ANALOG_READ)
    {
      data.fvalue = _digital[data.registre].dvalue;

      fSuccess = WriteFile(
          hPipe,              // handle to pipe
          &data,              // buffer to write from
          sizeof(datastruct), // number of bytes to write
          &cbWritten,         // number of bytes written
          NULL);              // not overlapped I/O

      if (!fSuccess || cbWritten != sizeof(datastruct))
      {
        printf("Erreur d'�criture, GLE=%lu.\n", GetLastError());
        break;
      }
    }
    else if (data.method == OP_ANALOG_WRITE)
    {
      analogWrite(data.registre, data.fvalue);
    }
  }

  FlushFileBuffers(hPipe);
  DisconnectNamedPipe(hPipe);
  CloseHandle(hPipe);

  printf("Fin du programme.\n");
  return 1;
}

int CreateMaquetteThread()
{
  HANDLE hMaquetteThread = NULL;
  DWORD dwMaquetteId = 0;

  hMaquetteThread = CreateThread(
      NULL,           // no security attribute
      0,              // default stack size
      MaquetteThread, // thread proc
      NULL,           // thread parameter
      0,              // not suspended
      &dwMaquetteId);

  if (hMaquetteThread == NULL)
  {
    printf("CreateThread failed, GLE=%lu.\n", GetLastError());
    return -1;
  }
  else
  {
    CloseHandle(hMaquetteThread);
  }

  return 0;
}

DWORD WINAPI MaquetteThread(LPVOID lpvParam)
{
  ChateauEauInit(mode);

  for (;;)
  {
    // **** Break loop if escape key is pressed
    if ((GetAsyncKeyState(VK_ESCAPE) & 0x01) || _digital[OUT_END].ivalue)
    {
      client_connected = 0;
      break;
    }

    // **** Beep
    if (_digital[OUT_BEEP].ivalue)
    {
      Beep(880, 750);
      _digital[OUT_BEEP].ivalue = false;
    }

    // **** Keyboard
    for (int i = 0; i < NB_KEYBOARD; i++)
    {
      _digital[_keyboard[i].input].ivalue = (GetAsyncKeyState(_keyboard[i].vKey) & 0x8000) != 0;
      _digital[_keyboard[i].input].raising = _digital[_keyboard[i].input].ivalue > _digital[_keyboard[i].input].memory;
      _digital[_keyboard[i].input].falling = _digital[_keyboard[i].input].ivalue < _digital[_keyboard[i].input].memory;
      _digital[_keyboard[i].input].memory = _digital[_keyboard[i].input].ivalue;
    }

    Sleep(100);

    ChateauEauProcess(mode);
  }

  ConsoleClose();
  return 1;
}
