#include <windows.h>

#define true 1
#define false 0

#define OUT_BEEP 254
#define OUT_END 255

void ConsoleInit();
void ConsoleClose();

void bargraph_at(COORD pos, double value);
void print_at(COORD pos, const char *str);
